from django.forms import ModelForm
from bills.models import Bill


class BillForm(ModelForm):
    class Meta:
        model = Bill
        fields = [
            "name",
            "due_date",
            "amount",
        ]

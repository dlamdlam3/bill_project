from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from bills.forms import BillForm
from bills.models import Bill


@login_required
def create_bill(request):
    if request.method == "POST":
        form = BillForm(request.POST)
        if form.is_valid():
            bill = form.save(commit=False)
            bill.owner = request.user
            bill.save()
            return redirect("home")
    else:
        form = BillForm()

    context = {
        "form": form,
    }
    return render(request, "bills/create.html", context)


@login_required
def bill_list(request):
    bills = Bill.objects.filter(owner=request.user)
    total_amount = bills.aggregate(Sum('amount'))['amount__sum']
    context = {
        "bill_list": bills,
        "total_amount": total_amount,
    }
    return render(request, "bills/list.html", context)


def edit_bill(request, bill_id):
    bill = get_object_or_404(Bill, id=bill_id)
    if request.method == 'POST':
        form = BillForm(request.POST, instance=bill)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = BillForm(instance=bill)
    return render(request, 'bills/edit_bill.html', {'form': form})


def remove_bill(request, bill_id):
    bill = get_object_or_404(Bill, id=bill_id)
    if request.method == 'POST':
        bill.delete()
        return redirect('home')
    return render(request, 'bills/remove_bill.html', {'bill': bill})

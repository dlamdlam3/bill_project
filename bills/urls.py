from django.urls import path
from bills.views import create_bill, bill_list, edit_bill, remove_bill


urlpatterns = [
    path("", bill_list, name="home"),
    path("create/", create_bill, name="create_bill"),
    path('bill/<int:bill_id>/edit/', edit_bill, name='edit_bill'),
    path('bill/<int:bill_id>/remove/', remove_bill, name='remove_bill'),
]

from django.db import models
from django.conf import settings


class Bill(models.Model):
    name = models.CharField(max_length=50)
    due_date = models.DateField()
    amount = models.DecimalField(max_digits=6, decimal_places=2)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )
